$(document).ready(function() {
	var chatInterval = 250;
	var $userName = $("#userName");
	var $chatOutput = $("#chatOutput");
	var $logout = $("#logout");
	var $chatInput = $("#chatInput");
	var $chatSend = $("#chatSend");


	function scrollDown() {
		document.getElementById("chatOutput").scrollTop = document.getElementById("chatOutput").scrollHeight;
	}

	function sendMessage() {
		var userNameString = $userName.val();
		var chatInputString = $chatInput.val();

			if (userNameString.length < 3 || chatInputString.length == 0)
			{
				alert('Username is too short or there is no message');
			} else {
				$.get("../php/write.php", {
					username: userNameString,
					text: chatInputString
				});
			}

			$chatInput.val("");
			retrieveMessages();
			setTimeout(function() {
				scrollDown();
			}, 100);
	}

	function retrieveMessages() {
			$.get("../php/read.php", function(data) {
				$chatOutput.html(data); // Paste content into chat output
			});
	}

	document.onkeydown = function(evt) {
		var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
		if (keyCode == 13)
			sendMessage();
	}

	$logout.click(function() {
		$.get("../php/logout.php", {});
		setTimeout(function() {
			location.reload();
		}, 100);
	});

	$chatSend.click(function() {
		sendMessage();
	});

	setTimeout(function() {
		scrollDown();
	}, 300);

	setInterval(function() {
		retrieveMessages();
	}, chatInterval);
});
