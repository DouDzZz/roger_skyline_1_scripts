<?php
require('./php/_connect.php');
if(isset($_POST['connexion']))
{
	if(empty($_POST['username']))
	{
		echo "There is no usename";
	}
	else 
	{
		if (empty($_POST['password']))
		{
			echo "There is no password.";
		}
		else
		{
			$username = htmlentities($_POST['username'], ENT_QUOTES, "ISO-8859-1");
			$password = htmlentities($_POST['password'], ENT_QUOTES, "ISO-8859-1");
			$mysqli = mysqli_connect('localhost', 'db', 'db', 'chat');
			if (!$mysqli)
			{
				echo "Failed to connect to db";
			}
			else
			{
				$Request = mysqli_query($mysqli, "SELECT * FROM `user` WHERE username = '".$username."' AND password = '".$password."'");
				if (mysqli_num_rows($Request) == 0)
				{
					echo "Invalid username or password";
				}
				else
				{
					session_start();
					$_SESSION['username'] = $username;
					header('Location: php/chat_room.php');
					exit();
				}
			}
		}
	}
}
?>
<html>
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>My awesome chat</title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/main.css" type="text/css" media="screen" />
	</head>
	<body>
		<div id="loginDiv">
			<h2 id="title">Welcome to my private 42 chat</h2>
			<form id="login" action="index.php" method="post" accept-charset="UTF-8">
				<input
					autofocus
					id="username"
					name="username"
					type=text"
					placeholder="Username"
					maxlength="32"
					value=""
					required >
				<input
					id="password"
					type="password"
					name="password"
					placeholder="Password"
					maxlength="32"
					required
					value="" >
				<button id="connexion" type="submit" name="connexion" value="Connexion">let's go</button>
	
			</form>
		</div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/vue"></script>
	</body>
</html>
